<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDireccionClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccion_clientes', function (Blueprint $table) {
            $table->id();
            $table->integer('id_cliente');
            $table->string('nombre');
            $table->string('apellido');
            $table->integer('telefono');
            $table->integer('dni');
            $table->integer('ruc')->nullable();
            $table->string('calle');
            $table->string('referencia')->nullable();
            $table->integer('departamento');
            $table->integer('provincia');
            $table->integer('distrito');
            $table->string('pais');
            $table->integer('codigo_postal');
            $table->integer('defecto')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direccion_clientes');
    }
}
