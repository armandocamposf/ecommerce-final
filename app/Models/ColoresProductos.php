<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ColoresProductos extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_producto',
        'id_color'
    ];

    public function detalle()
    {
        return $this->hasOne(Clores::class, 'id', 'id_color');
    }
}
