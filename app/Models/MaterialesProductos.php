<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaterialesProductos extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_producto',
        'id_material'
    ];

    public function detalle()
    {
        return $this->hasOne(Materiales::class, 'id', 'id_material');
    }
}
