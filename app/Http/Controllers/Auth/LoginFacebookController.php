<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

use Socialite;
use Auth;

class LoginFacebookController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function redirectGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callbackGoogle()
    {
        $user = Socialite::driver('google')->user();
        $val = User::whereEmail($user->getEmail())->count();
        if ($val > 0) {
            $use = User::whereEmail($user->getEmail())->first();
            Auth::loginUsingId($use->id);
            return redirect()->route('micuenta');
        } else {
            $use = new User();
            $use->name =
                $user->getName();
            $use->email =
                $user->getEmail();
            $use->tipoUsuario = 2;
            $use->password =
                sha1(12345);
            $use->provider = "facebook";
            $use->save();

            Auth::loginUsingId($use->id);
            return redirect()->route('micuenta');
        }
    }

    public function callback()
    {
        $user = Socialite::driver('facebook')->user();
        $val = User::whereEmail($user->getEmail())->count();
        if($val > 0)
        {
            $use = User::whereEmail($user->getEmail())->first();
            Auth::loginUsingId($use->id);
            return redirect()->route('micuenta');
        }
        else{
            $use = new User();
            $use->name =
            $user->getName();
            $use->email =
            $user->getEmail();
            $use->tipoUsuario = 2;
            $use->password =
            sha1(12345);
            $use->provider = "facebook";
            $use->save();

            Auth::loginUsingId($use->id);
            return redirect()->route('micuenta');
        }

    }
}
