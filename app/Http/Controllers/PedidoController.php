<?php

namespace App\Http\Controllers;

use App\Models\{Deprtamento, RelacionPedidoProducto, Producto, Pedido};
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class PedidoController extends Controller
{
    public function addProducto($idProducto, $idVariacion)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $ip = curl_exec($ch);
        curl_close($ch);

        $vali = Pedido::whereIpPedido($ip)->whereEstado(0)->count();

        if ($vali == 0) {
            $pedido = Pedido::create([
                'id_cliente' => 0,
                'ip_pedido' => $ip,
                'estado' => 0,
                'subtotal' => 0,
                'total' => 0,
                'tracking' => 0
            ]);
        } else {
            $pedido = Pedido::whereIpPedido($ip)->whereEstado(0)->first();
        }

        $vali2 = RelacionPedidoProducto::whereIdPedido($pedido->id)
            ->whereIdProducto($idProducto)
            ->whereIdVariacion($idVariacion)
            ->count();

        if ($vali2 > 0) {
            $producto = RelacionPedidoProducto::whereIdPedido($pedido->id)
                ->whereIdProducto($idProducto)
                ->whereIdVariacion($idVariacion)
                ->first();
            $producto->cantidad = $producto->cantidad + 1;
            $producto->save();
        } else {
            $producto = RelacionPedidoProducto::create([
                'id_pedido' => $pedido->id,
                'id_producto' => $idProducto,
                'id_variacion' => $idVariacion,
                'cantidad' => 1
            ]);
        }

        $productos = RelacionPedidoProducto::whereIdPedido($pedido->id)->with('detalle')->get();
        $total = 0;
        foreach ($productos as $producto) {
            $t = $producto->detalle->precio * $producto->cantidad;
            $total = $total + $t;
        }
        $pedido->total = $total;
        $pedido->save();

        return 200;
    }

    public function pedidosPA()
    {
        $pedidos = Pedido::whereEstado(2)->with('cliente')->get();

        return view('admin.pedidos', compact('pedidos'));
    }

    public function pedidos()
    {
        $pedidos = Pedido::where('estado', '>', 0)->with('cliente')->get();

        return view('admin.pedidos', compact('pedidos'));
    }

    public function detallePedido($idPedido)
    {
        $pedido = Pedido::whereId($idPedido)
            ->with([
                'relacionpedido',
                'relacionpedido.detalle',
                'relacionpedido.imagenes',
                'relacionpedido.color_detalle',
                'relacionpedido.talla_detalle',
                'relacionpedido.material_detalle',
                'cupon_detalle',
                'despachado'
            ])
            ->first();


        return view('admin.detallePedido', compact('pedido'));
    }

    public function updateEstado($idPedido, $estado)
    {
        $pedido = Pedido::find($idPedido);
        $pedido->estado = $estado;


        if ($estado == 4) {
            $pedido->despachado_por = Auth::user()->id;
        }

        $pedido->save();


        if ($estado == 3) {
            $productos = RelacionPedidoProducto::where('id_pedido', $pedido->id)->get();


            foreach ($productos as $producto) {

                $cantidad = $producto->cantidad;
                $producto = Producto::find($producto->id_producto);
                $producto->stock = $producto->stock - $cantidad;
                $producto->save();
            }
        }

        return 200;
    }

    public function newData()
    {
        $departamentos = Deprtamento::all();
        $productos = Producto::all();
        return view('admin.data.newPedido', compact('departamentos', 'productos'));
    }

    public function storeProductoPedido(Request $request)
    {
        $vali2 = RelacionPedidoProducto::whereIdPedido($request->idPedido)
            ->whereIdVariacion($request->material)
            ->count();

        if ($vali2 > 0) {
            $producto = RelacionPedidoProducto::whereIdPedido($request->idPedido)
                ->whereIdVariacion($request->material)
                ->first();
            $producto->cantidad = $request->cantidad;
            $producto->save();
        } else {
            $producto = RelacionPedidoProducto::create([
                'id_pedido' => $request->idPedido,
                'id_producto' => $request->idProducto,
                'id_variacion' => $request->material,
                'cantidad' => $request->cantidad
            ]);
        }
        $pedido = Pedido::find($request->idPedido);
        $productos = RelacionPedidoProducto::whereIdPedido($request->idPedido)->with('detalle')->get();
        $total = 0;
        foreach ($productos as $producto) {
            $t = $producto->detalle->precio * $producto->cantidad;
            $total = $total + $t;
        }
        $pedido->total = $total;
        $pedido->save();



        $relacion = Pedido::whereId($request->idPedido)
            ->with([
                'relacionpedido',
                'relacionpedido.detalle',
                'relacionpedido.variacion',
                'relacionpedido.variacion.color_detalle',
                'relacionpedido.variacion.material_detalle',
                'relacionpedido.variacion.talla_detalle',
                'cupon_detalle'
            ])
            ->first();

        $html = "";

        foreach ($relacion->relacionpedido as $producto) {
            $html .= "<tr>";
            $html .=                "<td>" . $producto->detalle->titulo . "</td>";
            $html .=                "<td>" . $producto->variacion->color_detalle->color . "</td>";
            $html .=                "<td>" . $producto->variacion->material_detalle->material . "</td>";
            $html .=                "<td>" . $producto->variacion->talla_detalle->talla . "</td>";
            $html .=                "<td>" . $producto->cantidad . "</td>";
            $html .=                "<td>" . $producto->detalle->precio . "</td>";
            $html .=                "<td>" . $producto->cantidad * $producto->detalle->precio . "</td>";
            $html .=            "</tr>";
        }

        return response()->json([
            'relacion' => $relacion,
            'html' => $html,
            'total' => $pedido->total
        ]);
    }

    public function storePedido($idPedido)
    {
        $pedido = Pedido::find($idPedido);

        $productos = RelacionPedidoProducto::where('id_pedido', $pedido->id)->get();


        foreach ($productos as $producto) {
            $cantidad = $producto->cantidad;
            $producto = Producto::find($producto->id_producto);
            $producto->stock = $producto->stock - $cantidad;
            $producto->save();
        }

        $pedido->estado = 2;
        $pedido->save();

        return 200;
    }

    public function mispedidos()
    {
        $pedidos = Pedido::whereCreadoPor(Auth::user()->id)
            ->whereEstado(1)
            ->with('cliente')
            ->get();

        return view('admin.pedidos', compact('pedidos'));
    }

    public function mispedidosp()
    {
        $pedidos = Pedido::whereCreadoPor(Auth::user()->id)
            ->whereEstado(2)
            ->with('cliente')
            ->get();

        return view('admin.pedidos', compact('pedidos'));
    }

    public function mispedidospp()
    {
        $pedidos = Pedido::whereEstado(1)
            ->with('cliente')
            ->get();

        return view('admin.pedidos', compact('pedidos'));
    }

    public function mispedidosdes()
    {
        $pedidos = Pedido::whereEstado(4)
            ->whereDespachadoPor(Auth::user()->id)
            ->with('cliente')
            ->get();

        return view('admin.pedidos', compact('pedidos'));
    }
}
