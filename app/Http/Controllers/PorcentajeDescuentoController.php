<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PorcentajeDescuento;

class PorcentajeDescuentoController extends Controller
{

    public function index()
    {
        return view('admin.descuento', [
            'descuento' => PorcentajeDescuento::find(1)->porcentaje
        ]);
    }



    public function store(Request $request)
    {
        $pro = PorcentajeDescuento::find(1);
        $pro->porcentaje = $request->descuento;
        $pro->save();

        return back()->with('success', 'Porcentaje Actualizado con exito');
    }
}
