<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Inicio</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.rels')
</head>

<body>
    <!--[if lt IE 8]>
 <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->
        <!--Slider Area Start-->
        <section class="slider-area mb-50">
            <div class="slider-wrapper theme-default">
                <!--Slider Background Image Start-->
                <div id="slider" class="nivoSlider">
                    <img src="img/slider/5.jpg" alt="" title="#htmlcaption" />
                    <img src="img/slider/6.jpg" alt="" title="#htmlcaption2" />
                    <img src="img/slider/7.jpg" alt="" title="#htmlcaption3" />
                </div>
                <!--Slider Background Image End-->
                <!--1st Slider Caption Start-->
                <div id="htmlcaption" class="nivo-html-caption">
                    <div class="slider-caption">
                        <div class="slider-text">
                            <h5 class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">Oferta
                                Exclusiva -60% Off esta semana </h5>
                            <h1 class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">Colección
                                <br><span>Invierno 60% Off</span></h1>
                            <h4 class="wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">Precios
                                desde <span>S/ 39.90</span></h4>
                            <div class="slider-button">
                                <a href="#" class="wow button animated fadeInLeft" data-text="Shop now"
                                    data-wow-duration="2.5s" data-wow-delay="0.5s">Comprar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--1st Slider Caption End-->
                <!--2nd Slider Caption Start-->
                <div id="htmlcaption2" class="nivo-html-caption">
                    <div class="slider-caption">
                        <div class="slider-text">
                            <h5 class="wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                Exclusive Offer -20% Off This Week </h5>
                            <h1 class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">Sale
                                Keyboard <br><span>Up To 20% Off</span></h1>
                            <h4 class="wow animated fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s">Starting
                                at <span>$260.99</span></h4>
                            <div class="slider-button">
                                <a href="#" class="wow button animated fadeInLeft" data-text="Shop now"
                                    data-wow-duration="2.5s" data-wow-delay="0.5s">shopping Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--2nd Slider Caption End-->
                <!--3rd Slider Caption Start-->
                <div id="htmlcaption3" class="nivo-html-caption">
                    <div class="slider-caption">
                        <div class="slider-text">
                            <h5 class="wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                Bestseller Products 2017 - 2018 </h5>
                            <h1 class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">Awesome
                                Deal!<br><span>Gamepad Gbox </span></h1>
                            <h4 class="wow animated fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s">Starting
                                at <span>$120.99</span></h4>
                            <div class="slider-button">
                                <a href="#" class="wow button animated fadeInLeft" data-text="Shop now"
                                    data-wow-duration="2.5s" data-wow-delay="0.5s">shopping Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--3rd Slider Caption End-->
            </div>
        </section>

        
        <!--Slider Area End-->
        <!--Offer Image Area Start-->
        <div class="offer-area mb-50">
            <div class="container">
                <div class="row">
                    <!--Single Offer Start-->
                    <div class="col-lg-4 col-md-4">
                        <div class="single-offer">
                            <div class="offer-img img-full">
                                <a href="{{ route('welcome') }}/video">
                                    <img src="{{ route('welcome') }}/public/img/publicidad/video-youtube1-min.jpg" alt="video top semanal">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--Single Offer End-->
                    <!--Single Offer Start-->
                    <div class="col-lg-4 col-md-4">
                        <div class="single-offer">
                            <div class="offer-img img-full">
                                <a href="{{ route('welcome') }}/video">
                                    <img src="{{ route('welcome') }}/public/img/publicidad/video-youtube2-min.jpg" alt="video top semanal">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--Single Offer End-->
                    <!--Single Offer Start-->
                    <div class="col-lg-4 col-md-4">
                        <div class="single-offer">
                            <div class="offer-img img-full">
                                <a href="{{ route('welcome') }}/video">
                                    <img src="{{ route('welcome') }}/public/img/publicidad/video-youtube3-min.jpg" alt="video top semanal">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--Single Offer End-->
                </div>
            </div>
        </div>
        <!--Offer Image Area End-->

        <section class="hot-deal-product-of-the-day mb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--Section Title1 Start-->
                        <div class="section-title1-border">
                            <div class="section-title1">
                                <h3>Llévalo hoy</h3>
                            </div>
                        </div>
                        <!--Section Title1 End-->
                    </div>
                </div>
                <div class="row">
                    <div class="hot-deal-of-product owl-carousel">
                        <!--Single Product Start-->
                        <div class="col-lg-12">
                            <div class="single-product hot-deal-list">
                                <div class="product-img">
                                    <a href="#">
                                        <img class="first-img" src="https://imagenes.luneperu.com/629387dbe82f2_1653835739.jpg" alt="producto {{ config('app.name') }} llevalo hoy oferta">
                                        <img class="hover-img" src="https://imagenes.luneperu.com/629387dbee3e7_1653835739.jpg" alt="producto {{ config('app.name') }} llevalo hoy oferta">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="pro-title"><a href="single-product.html">Vestido Silvana</a></h2>
                                    <div class="pro-rating-price">
                                        <div class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div class="product-price">
                                            <span class="old-price">S/84.00</span>
                                            <span class="new-price">S/65.00</span>
                                        </div>
                                        <div class="hot-deal-product-des">
                                            <p>Modelo exclusivo de nuestra tienda, llévalo hoy no dejes pasar esta oportunidad.</p>
                                        </div>
                                        <div class="count-down-box">
                                            <div class="count-box">
                                                <div class="pro-countdown" data-countdown="2022/06/07"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Single Product End-->
                        <!--Single Product Start-->
                        <div class="col-lg-12">
                            <div class="single-product hot-deal-list">
                                <div class="product-img">
                                    <a href="#">
                                        <img class="first-img" src="https://imagenes.luneperu.com/629387dbe82f2_1653835739.jpg" alt="producto {{ config('app.name') }} llevalo hoy oferta">
                                        <img class="hover-img" src="https://imagenes.luneperu.com/629387dbee3e7_1653835739.jpg" alt="producto {{ config('app.name') }} llevalo hoy oferta">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="pro-title"><a href="single-product.html">Vestido Silvana</a></h2>
                                    <div class="pro-rating-price">
                                        <div class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div class="product-price">
                                            <span class="old-price">S/84.00</span>
                                            <span class="new-price">S/65.00</span>
                                        </div>
                                        <div class="hot-deal-product-des">
                                            <p>Modelo exclusivo de nuestra tienda, llévalo hoy no dejes pasar esta oportunidad.</p>
                                        </div>
                                        <div class="count-down-box">
                                            <div class="count-box">
                                                <div class="pro-countdown" data-countdown="2022/06/07"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Single Product End-->
                        <!--Single Product Start-->


                    </div>
                </div>
            </div>
        </section>


        <!--Offer Image Area End-->
        <!--New Arrival Product Start-->
        <section class="new-arrival-product mb-20">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--Section Title1 Start-->
                        <div class="section-title1-border">
                            <div class="section-title1">
                                <h3>Tienda</h3>
                            </div>
                        </div>
                        <!--Section Title1 End-->
                    </div>
                </div>

                <!--Product Tab Start-->
                <div class="tab-content">
                    <div id="laptop" class="tab-pane fade show active">
                        <div class="row">
                            <div class="new-arrival-list-product owl-carousel">

                                <!--Single Product Start-->
                                @if($productos != null)
                                @foreach ($productos as $producto)
                                    <div class="col-lg-12">
                                        <div class="row no-gutters single-product style-2">
                                            <div class="col-4">
                                                <div class="product-img">
                                                    <a href="/producto/{{strtolower(str_replace(" ", "-", $producto->titulo))}}">
                                                        <img class="first-img" src="https://imagenes.luneperu.com/{{$producto->imagenes->imagen}}" alt="{{$producto->titulo}}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="product-content">
                                                    <h2 style="font-weight: 500!important;color:#040404;"><a href="/producto/{{strtolower(str_replace(" ", "-", $producto->titulo))}}">{{$producto->titulo}}</a></h2>

                                                    <div class="product-price">
                                                        <span class="new-price">S/ {{$producto->precio}}</span>
                                                    </div>
                                                    
                                                    <p>{{$producto->descripcion}}</p>
                                                    <span class="badge badge-light">Mujer</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                                <!--Single Product End-->



                            </div>
                        </div>
                    </div>

                </div>
                <!--Product Tab End-->
            </div>
        </section>
        <!--New Arrival Product End-->

        <section class="corporate-about white-bg pb-30">
            <div class="container">
                <div class="row all-about no-gutters">
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="single-about">
                            <div class="block-wrapper">
                                <div class="about-content">
                                    <h5 style="font-size: 13px;">Diseños Exclusivos</h5>
                                    <p>Siempre a la moda</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="single-about">
                            <div class="block-wrapper2">
                                <div class="about-content">
                                    <h5 style="font-size: 13px;">100% algodón </h5>
                                    <p>Materiales de Calidad </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="single-about">
                            <div class="block-wrapper3">
                                <div class="about-content">
                                    <h5 style="font-size: 13px;">Precios Bajos</h5>
                                    <p>Precios bajos siempre</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="single-about not-border">
                            <div class="block-wrapper4">
                                <div class="about-content">
                                    <h5 style="font-size: 13px;">Compra segura</h5>
                                    <p>Compra 100% segura</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

        <Section class="latest-posts-blog-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--Section Title1 Start-->
                        <div class="section-title1-border">
                            <div class="section-title1">
                                <h3>Publicaciones</h3>
                            </div>
                        </div>
                        <!--Section Title1 End-->
                    </div>
                </div>
                <!--Latest Blog Start-->
                <div class="latest-blog">
                    <div class="row">
                        <div class="col-lg-12 pb-3">
                            <div class="latest-blog-active owl-carousel">
                                <!--Single Latest Blog Start-->
                                <div class="our-single-blog">
                                    <div class="our-blog-img img-full">
                                        <a href="#">
                                            <img src="{{ route('welcome') }}/public/img/blog/tendencias1.jpg" alt="Principales tendencias Europa de Ropa">
                                        </a>
                                    </div>
                                    <div class="our-blog-content mt-15">
                                        <h3><a href="#">Principales tendencias Europa </a></h3>
                                        <span class="our-blog-author">por <a href="blog-author.html">{{ config('app.name') }}</a></span>
                                        <div class="our-blog-des mb-20">
                                            <p>Estas son las tiendas de ropa vintage aprobadas por las celebrities que debes conocer.</p>
                                        </div>
                                        {{-- <div class="blog-meta">
                                            <span class="post-comment"><i class="ion-clipboard"></i>3 comentarios</span>
                                            <span class="blog-post-date pull-right">24/05/2022</span>
                                        </div> --}}
                                    </div>
                                </div>


                                <div class="our-single-blog">
                                    <div class="our-blog-img img-full">
                                        <a href="#">
                                            <img src="{{ route('welcome') }}/public/img/blog/tendencias2.jpg" alt="Tips para compromisos exclusivos">
                                        </a>
                                    </div>
                                    <div class="our-blog-content mt-15">
                                        <h3><a href="#"> Tips para compromisos exclusivos</a></h3>
                                        <span class="our-blog-author">By <a href="blog-author.html">{{ config('app.name') }}</a></span>
                                        <div class="our-blog-des mb-20">
                                            <p>7 ideas y consejos: dónde comprometerse y lista de chequeo, para un compromiso exitoso.</p>
                                        </div>
                                        {{-- <div class="blog-meta">
                                            <span class="post-comment"><i class="ion-clipboard"></i>3 comentarios</span>
                                            <span class="blog-post-date pull-right">23/05/2022</span>
                                        </div> --}}
                                    </div>
                                </div>

                                <!--
                                <div class="our-single-blog">
                                    <div class="our-blog-img img-full">
                                        <a href="blog-post-audio.html">
                                            <img src="img/blog/3.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="our-blog-content mt-15">
                                        <h3><a href="blog.html">Post with Audio</a></h3>
                                        <span class="our-blog-author">By <a href="blog-author.html">admin</a></span>
                                        <div class="our-blog-des mb-20">
                                            <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium</p>
                                        </div>
                                        <div class="blog-meta">
                                            <span class="post-comment"><i class="ion-clipboard"></i>3 comments</span>
                                            <span class="blog-post-date pull-right">10 Mar 2015</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="our-single-blog">
                                    <div class="our-blog-img img-full">
                                        <a href="blog-post-video.html">
                                            <img src="img/blog/4.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="our-blog-content mt-15">
                                        <h3><a href="blog-post-video.html">Post with Video</a></h3>
                                        <span class="our-blog-author">By <a href="blog-author.html">admin</a></span>
                                        <div class="our-blog-des mb-20">
                                            <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium</p>
                                        </div>
                                        <div class="blog-meta">
                                            <span class="post-comment"><i class="ion-clipboard"></i>3 comments</span>
                                            <span class="blog-post-date pull-right">10 Mar 2015</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="our-single-blog">
                                    <div class="our-blog-img img-full">
                                        <a href="blog-maecenas-ultricies.html">
                                            <img src="img/blog/5.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="our-blog-content mt-15">
                                        <h3><a href="blog-maecenas-ultricies.html">Maecenas ultricies</a></h3>
                                        <span class="our-blog-author">By <a href="blog-author.html">admin</a></span>
                                        <div class="our-blog-des mb-20">
                                            <p>Donec vitae hendrerit arcu, sit amet faucibus nisl. Cras pretium</p>
                                        </div>
                                        <div class="blog-meta">
                                            <span class="post-comment"><i class="ion-clipboard"></i>3 comments</span>
                                            <span class="blog-post-date pull-right">10 Mar 2015</span>
                                        </div>
                                    </div>
                                </div> -->



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </Section>

       

        <!--Footer Area Start-->
        @include('layouts.footer')
        <!--Footer Area End-->
        <!--Modal Start-->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal Content Strat-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-details">
                            <div class="row">
                                <!--Product Img Strat-->
                                <div class="col-xl-5 col-lg-5">
                                    <!--Product Tab Content Start-->
                                    <div class="tab-content">
                                        <div id="watch1" class="tab-pane fade show active">
                                            <div class="modal-img img-full">
                                                <img src="img/single-product/large/1.jpg" alt="">
                                            </div>
                                        </div>
                                        <div id="watch2" class="tab-pane fade">
                                            <div class="modal-img img-full">
                                                <img src="img/single-product/large/2.jpg" alt="">
                                            </div>
                                        </div>
                                        <div id="watch3" class="tab-pane fade">
                                            <div class="modal-img img-full">
                                                <img src="img/single-product/large/3.jpg" alt="">
                                            </div>
                                        </div>
                                        <div id="watch4" class="tab-pane fade">
                                            <div class="modal-img img-full">
                                                <img src="img/single-product/large/4.jpg" alt="">
                                            </div>
                                        </div>
                                        <div id="watch5" class="tab-pane fade">
                                            <div class="modal-img img-full">
                                                <img src="img/single-product/large/5.jpg" alt="">
                                            </div>
                                        </div>
                                        <div id="watch6" class="tab-pane fade">
                                            <div class="modal-img img-full">
                                                <img src="img/single-product/large/6.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <!--Product Tab Content End-->
                                    <!--Single Product Tab Menu Start-->
                                    <div class="modal-product-tab">
                                        <ul class="nav">
                                            <li><a class="active" data-toggle="tab" href="#watch1"><img
                                                        src="img/product-thumb/1.jpg" alt=""></a></li>
                                            <li><a data-toggle="tab" href="#watch2"><img src="img/product-thumb/3.jpg"
                                                        alt=""></a></li>
                                            <li><a data-toggle="tab" href="#watch3"><img src="img/product-thumb/2.jpg"
                                                        alt=""></a></li>
                                            <li><a data-toggle="tab" href="#watch4"><img src="img/product-thumb/4.jpg"
                                                        alt=""></a></li>
                                        </ul>
                                    </div>
                                    <!--Single Product Tab Menu Start-->
                                </div>
                                <!--Product Img End-->
                                <!-- Product Content Start-->
                                <div class="col-xl-7 col-lg-7">
                                    <div class="product-info">
                                        <h2>Natural passages</h2>
                                        <div class="product-price">
                                            <span class="old-price">$74.00</span>
                                            <span class="new-price">$69.00</span>
                                        </div>
                                        <a href="#" class="see-all">See all features</a>
                                        <div class="add-to-cart quantity">
                                            <form class="add-quantity" action="#">
                                                <div class="quantity modal-quantity">
                                                    <label>Quantity</label>
                                                    <input type="number">
                                                </div>
                                                <div class="add-to-link">
                                                    <button class="form-button" data-text="add to cart">add to
                                                        cart</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="cart-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                                veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et
                                                mattis vulputate, tristique ut lectus.</p>
                                        </div>
                                        <div class="social-share">
                                            <h3>Share this product</h3>
                                            <ul class="socil-icon2">
                                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                                <li><a href=""><i class="fa fa-twitter"></i></a></li>

                                                <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--Product Content End-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--Modal Content Strat-->
            </div>
        </div>
        <!--Modal End-->
    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="js/vendor/modernizr-3.6.0.min.js"></script>
    <script src="js/vendor/jquery-3.6.0.min.js"></script>
    <script src="js/vendor/jquery-migrate-3.3.2.min.js"></script>
    <!--Popper-->
    <script src="js/popper.min.js"></script>
    <!--Bootstrap-->
    <script src="js/bootstrap.min.js"></script>
    <!--Imagesloaded-->
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <!--Isotope-->
    <script src="js/isotope.pkgd.min.js"></script>
    <!--Ui js-->
    <script src="js/jquery-ui.min.js"></script>
    <!--Countdown-->
    <script src="js/jquery.countdown.min.js"></script>
    <!--Counterup-->
    <script src="js/jquery.counterup.min.js"></script>
    <!--ScrollUp-->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!--Chosen js-->
    <script src="js/chosen.jquery.js"></script>
    <!--Meanmenu js-->
    <script src="js/jquery.meanmenu.min.js"></script>
    <!--Instafeed-->
    <script src="js/instafeed.min.js"></script>
    <!--EasyZoom-->
    <script src="js/easyzoom.min.js"></script>
    <!--Fancybox-->
    <script src="js/jquery.fancybox.pack.js"></script>
    <!--Nivo Slider-->
    <script src="js/jquery.nivo.slider.js"></script>
    <!--Waypoints-->
    <script src="js/waypoints.min.js"></script>
    <!--Carousel-->
    <script src="js/owl.carousel.min.js"></script>
    <!--Slick-->
    <script src="js/slick.min.js"></script>
    <!--Wow-->
    <script src="js/wow.min.js"></script>
    <!--Plugins-->
    <script src="js/plugins.js"></script>
    <!--Main Js-->
    <script src="js/main.js"></script>
</body>

</html>
