<?php $total_pedido = 0; ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Carrito de Compra</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.rels')

</head>

<body>

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->

        <section class="heading-banner-area pt-30">
		    <div class="container">
		        <div class="row">
		            <div class="col-lg-12">
		                <div class="heading-banner">
		                    <div class="breadcrumbs">
		                        <ul>
		                            <li><a href="index.html">Inicio</a><span class="breadcome-separator">></span></li>
		                            <li>Nosotros</li>
		                        </ul>
		                    </div>
		                    <div class="heading-banner-title">
		                        <h1>Nosotros</h1>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>


        <section class="about-us-area">

            <div class="about-us-img bg-4"></div>

		    <div class="container-fluid">
		        <div class="row">

                    <div class="col-lg-6 offset-lg-6 col-md-6 offset-md-6 about-us-content">
                        <div class="about-us-title text-center">
                            <h2><strong>¿QUIÉNES </strong><strong>SOMOS?</strong></h2>
                        </div>
                        <p>Somos una marca 100% PERUANA con más de 7 años en el mercado, dedicada al diseño, confección y venta de prendas exclusivas dirigidas a mujeres fuertes y empoderadas que buscan verse y sentirse únicas con nuestros diseños. <br><br> De otro lado, contamos con más de 64 mil seguidores en redes sociales, los cuales respaldan el buen desempeño de nuestro equipo de trabajo el cual busca promover nuestro crecimiento logrando así crear nuevos puestos de trabajo.</p>

                    </div>

		        </div>
		    </div>
		</section>

        <section class="counter-up-area">
            <div class="row no-gutters">
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-count-box">
                        <div class="counter-img">
                            <img src="img/icon/6.png" alt="">
                        </div>
                        <div class="counter-content">
                            <div class="counter-number">
                                <h2><span class="counter">1234</span></h2>
                            </div>
                            <div class="counter-title">
                                <h5>Clientes</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Single Count Box End-->
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-count-box">
                        <div class="counter-img">
                            <img src="img/icon/7.png" alt="">
                        </div>
                        <div class="counter-content">
                            <div class="counter-number">
                                <h2><span class="counter">145</span></h2>
                            </div>
                            <div class="counter-title">
                                <h5>Moda</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Single Count Box End-->
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-count-box">
                        <div class="counter-img">
                            <img src="img/icon/8.png" alt="">
                        </div>
                        <div class="counter-content">
                            <div class="counter-number">
                                <h2><span class="counter">1234</span></h2>
                            </div>
                            <div class="counter-title">
                                <h5>Tiempo</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Single Count Box End-->
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-count-box">
                        <div class="counter-img">
                            <img src="img/icon/9.png" alt="">
                        </div>
                        <div class="counter-content">
                            <div class="counter-number">
                                <h2><span class="counter">98366</span></h2>
                            </div>
                            <div class="counter-title">
                                <h5>Ventas</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Single Count Box End-->
            </div>
		</section>

        <!--Footer Area Start-->
        @include('layouts.footer')
        <!--Footer Area End-->
    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-migrate-3.3.2.min.js') }}"></script>
    <!--Popper-->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!--Bootstrap-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--Imagesloaded-->
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!--Isotope-->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!--Countdown-->
    <script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!--ScrollUp-->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!--Chosen js-->
    <script src="{{ asset('js/chosen.jquery.js') }}"></script>
    <!--Meanmenu js-->
    <script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
    <!--Instafeed-->
    <script src="{{ asset('js/instafeed.min.js') }}"></script>
    <!--EasyZoom-->
    <script src="{{ asset('js/easyzoom.min.js') }}"></script>
    <!--Fancybox-->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!--Nivo Slider-->
    <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
    <!--Waypoints-->
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <!--Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!--Slick-->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!--Wow-->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!--Plugins-->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!--Main Js-->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('jquery.flexslider.js') }}"></script>
</body>

</html>
