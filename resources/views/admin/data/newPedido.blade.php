@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Generar Nuevo Pedido

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-20">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong> {{ $message }} </strong>
                </div>
            @endif
            <div class="col-md-12" id="RegCliente">
                <h4>Datos del Cliente</h4>
                <form id="DataCliente">
                    @csrf
                    <div class="form-group">
                        <label for="">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>

                    <div class="form-group">
                        <label for="">Apellido</label>
                        <input type="text" class="form-control" id="apellido" name="apellido">
                    </div>

                    <div class="form-group">
                        <label for="">Direccion</label>
                        <input type="text" class="form-control" id="direccion" name="direccion">
                    </div>

                    <div class="form-group">
                        <label for="">Apartamento</label>
                        <input type="text" class="form-control" id="apartamento" name="apartamento">
                    </div>

                    <div class="col-md-6">
                        <p><label>Departamento<span class="required">*</span></label></p>
                        <select name="departamento" id="departamento" class="form-control" onchange="getProvincias()">
                            <option value="">SELECCIONE</option>
                            @foreach ($departamentos as $departamento)
                                <option value="{{ $departamento->id }}">
                                    {{ $departamento->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-6">
                        <p><label>Provincia<span class="required">*</span></label></p>
                        <select name="provincia" id="provincia" class="form-control" onchange="getDistritos()">
                            <option value="">SELECCIONE</option>
                        </select>
                    </div>

                    <div class="col-md-6">
                        <p><label>Distrito<span class="required">*</span></label></p>
                        <select name="distrito" class="form-control" id="distrito">
                            <option value="">SELECCIONE</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Telefono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono">
                    </div>

                    <div class="form-group">
                        <label for="">Correo</label>
                        <input type="text" class="form-control" id="correo" name="correo">
                    </div>
                </form>
                <button type="button" class="btn btn-success form-control" id="dataClient"> Guardar </button>

            </div>

            <div class="col-md-12" id="datosPedido" style="display: none">
                <table class="table ">
                    <thead class="thead-light">
                        <tr>
                            <td>Nombre Cliente</td>
                            <td>Correo Cliente</td>
                            <td>Id Cliente</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="nombreCliente"></td>
                            <td id="correoCliente"></td>
                            <td id="idCliente"></td>
                        </tr>
                    </tbody>
                </table>
                <br><br>
                <button class="btn btn-success" data-toggle="modal" data-target=".bd-example-modal-lg"> <i
                        class="fa fa-plus-circle"></i> Agregar Producto </button>
                <br>

                <table class="table table-striped">
                    <thead class="thead-light">
                        <tr>
                            <td>Producto</td>
                            <td>Color</td>
                            <td>Material</td>
                            <td>Talla</td>
                            <td>Cantidad</td>
                            <td>Precio</td>
                            <td>Total</td>
                        </tr>
                    </thead>
                    <tbody id="dataPedido">

                    </tbody>
                </table>

                <h1>Total: <span id="total"></span></h1>

                <button class="btn btn-success" onclick="storePedido()">Guardar Pedido</button>
            </div>

            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>


    <div class="modal fade bd-example-modal-lg" id="modalProductos" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-bordered table-hover mt-3" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>SKU</th>
                                <th>Producto</th>
                                <th>Stock</th>
                                <th>Precio</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($productos as $producto)
                                <tr class="odd gradeX">
                                    <td>{{ $producto->sku }}</td>
                                    <td>{{ $producto->titulo }}</td>
                                    <td>{{ $producto->stock }}</td>
                                    <td>S/ {{ $producto->precio }}</td>
                                    <td width="30px">
                                        <a href="#" onclick="agregarProducto({{ $producto->id }})"><i
                                                class="fa fa-plus-circle text-success" style="font-size: 30px"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Agregar Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="DataProducto">
                        <div class="form-group">
                            <label for="">Producto</label>
                            <input type="text" class="form-control" disabled id="producto">
                            <input type="hidden" class="form-control" name="idPedido" id="idPedido">
                            <input type="hidden" class="form-control" name="idProducto" id="idProducto">
                        </div>

                        <div class="form-group">
                            <label for="">Stock</label>
                            <input type="text" class="form-control" disabled id="stock">
                            <input type="hidden" class="form-control" id="stock2">
                        </div>

                        <div class="form-group">
                            <label for="">Colores</label>
                            <select class="form-control" name="color" id="color"
                                onchange="getTallas({{ $producto->id }})">
                                <option value="">SELECCIONE</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Tallas</label>
                            <select class="form-control" name="talla" id="talla"
                                onchange="getMateriales({{ $producto->id }})">
                                <option value="">SELECCIONE</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Materiales</label>
                            <select class="form-control" name="material" id="material" onchange="getStock()">
                                <option value="">SELECCIONE</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Cantidad</label>
                            <input type="text" class="form-control" name="cantidad" id="cantidad">
                        </div>

                        <div class="alert alert-danger alert-block mt-20" id="alertaStock" style="margin-top: 20px; display: none">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong> Cantidad No puede exceder el stock </strong>
                        </div>
                </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="storeDataProducto">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        const pedido = ""
        const user = ""

        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu: "Mostrar  _MENU_  Registros",
                }
            });
        });

        function getProvincias() {
            axios.get('/provincias/' + $("#departamento").val()).then((response) => {
                var provincias = $("#provincia");
                provincias.html(response.data.html)
            })
        }

        function getDistritos() {
            axios.get('/distritos/' + $("#provincia").val()).then((response) => {
                var distrito = $("#distrito");
                distrito.html(response.data.html)
            })
        }

        function getTallas() {
            idProducto = $("#idProducto").val()
            console.log(idProducto)
            color = $("#color").val();
            axios.get('/getTallas/' + idProducto + "/" + color).then((response) => {
                $("#talla").html(response.data.html);
            });
        }

        function getMateriales() {
            idProducto = $("#idProducto").val()
            color = $("#color").val()
            talla = $("#talla").val()
            axios.get('/getMateriales/' + idProducto + '/' + color + '/' + talla).then((response) => {
                $("#material").html(response.data.html)
            })
        }

        function getStock() {
            idVariante = $("#material").val()

            axios.get('/getStock/' + idVariante).then((response) => {
                $("#stock").val(response.data.stock)
                $("#stock2").val(response.data.stock)
            })
        }

        function agregarProducto(idProducto) {
            $("#exampleModalCenter").modal('show')
            $("#modalProductos").modal('hide')

            axios.get('/administrador/getAdicionalesProducto/' + idProducto).then((response) => {
                $("#color").html(response.data.colores)
                $("#producto").val(response.data.producto.titulo)
                $("#stock").val(response.data.producto.stock)
                $("#idProducto").val(idProducto)
            })
        }

        function storePedido() {
            axios.get('/administrador/storePedido/' + $("#idPedido").val()).then((response) => {
                if (response.data == 200) {
                    window.location.href = "/administrador/mispedidos"
                }
            })
        }

        $(document).ready(function() {
            $('#dataClient').on('click', function(e) {
                e.preventDefault();
                var dataString = $('#DataCliente').serialize();
                axios.post('/administrador/storeDataCliente', dataString).then((response) => {
                    $("#RegCliente").hide()
                    this.pedido = response.data.pedido
                    this.user = response.data.user
                    $("#idPedido").val(this.pedido.id)
                    $("#datosPedido").show()
                    $("#correoCliente").html(this.user.email)
                    $("#nombreCliente").html(this.user.name)
                    $("#idCliente").html(this.user.id)
                })
            });

            $('#storeDataProducto').on('click', function(e) {
                e.preventDefault();
                if ($("#cantidad").val() > $("#stock2").val()) {
                    $("#alertaStock").show()
                } else {
                    var dataString = $('#DataProducto').serialize();
                    axios.post('/administrador/storeProductoPedido', dataString).then((response) => {
                        $("#dataPedido").html(response.data.html)
                        $("#exampleModalCenter").modal('hide')
                        $("#cantidad").val()
                        $("#total").html(response.data.total)
                        formulario = document.getElementById('DataProducto');
                        formulario.reset();
                    })
                }

            });
        });
    </script>
@endsection
