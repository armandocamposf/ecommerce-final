@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Generar Nuevo Pedido

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-20">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong> {{ $message }} </strong>
                </div>
            @endif
            <div class="col-md-12" id="RegCliente">
                <h4>Datos del Producto</h4>
                <form id="DataCliente" method="post" action="{{route('updateProducto')}}">
                    @csrf
                    <div class="form-group">
                        <label for="">Producto</label>
                        <input type="text" class="form-control" id="nombre" value="{{ $producto->titulo }}"  name="titulo">
                        <input type="hidden" class="form-control" value="{{ $producto->id }}"  name="idProducto">
                    </div>

                    <div class="form-group">
                        <label for="">Marca</label>
                        <input type="text" class="form-control" value="{{ $producto->marca }}" id="apellido"
                            name="marca">
                    </div>

                    <div class="form-group">
                        <label for="">SKU</label>
                        <input type="text" class="form-control" value="{{ $producto->sku }}" id="direccion"
                            name="sku">
                    </div>

                    <div class="form-group">
                        <label for="">Precio</label>
                        <input type="text" class="form-control" value="{{ $producto->precio }}" id="apartamento"
                            name="precio">
                    </div>

                    <input type="submit" value="Actualizar Producto" class="btn btn-warning form-control" style="margin-bottom: 20px">
                </form>
            </div>

            <div class="col-md-12" id="datosPedido">

                <button class="btn btn-success" data-toggle="modal" data-target="#exampleModalCenter"> <i
                        class="fa fa-plus-circle"></i> Agregar Variacion </button>
                <br>

                <h3>Variaciones</h3>
                <br>

                <table class="table table-striped">
                    <thead class="thead-light">
                        <tr>
                            <td>Color</td>
                            <td>Talla</td>
                            <td>Material</td>
                            <td>Stock</td>
                        </tr>
                    </thead>
                    <tbody id="dataPedido">

                    </tbody>
                </table>


                <a href="{{route('productos')}}" class="btn btn-danger mx-auto form-control">Terminar</a>
            </div>

            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Agregar Variante</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="DataProducto">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="idProducto" value="{{ $producto->id }}"
                                id="idProducto">
                        </div>

                        <div class="form-group">
                            <label for="">Colores</label>
                            <select class="form-control" name="color" id="colores">
                                <option value="">SELECCIONE</option>
                                @foreach ($colores as $color)
                                    <option value="{{ $color->id }}"> {{ $color->color }} </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Tallas</label>
                            <select class="form-control" name="talla" id="tallas">
                                <option value="">SELECCIONE</option>

                                @foreach ($tallas as $talla)
                                    <option value="{{ $talla->id }}"> {{ $talla->talla }} </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Materiales</label>
                            <select class="form-control" name="material" id="materiales">
                                <option value="">SELECCIONE</option>
                                @foreach ($materiales as $material)
                                    <option value="{{ $material->id }}"> {{ $material->material }} </option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="">Stock</label>
                            <input type="text" class="form-control" name="cantidad" id="cantidad">
                        </div>
                </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="storeDataProducto">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        const pedido = ""
        const user = ""

        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu: "Mostrar  _MENU_  Registros",
                }
            });
        });

        function getProvincias() {
            axios.get('/provincias/' + $("#departamento").val()).then((response) => {
                var provincias = $("#provincia");
                provincias.html(response.data.html)
            })
        }

        function getDistritos() {
            axios.get('/distritos/' + $("#provincia").val()).then((response) => {
                var distrito = $("#distrito");
                distrito.html(response.data.html)
            })
        }


        function agregarProducto(idProducto) {
            $("#exampleModalCenter").modal('show')
            $("#modalProductos").modal('hide')

            axios.get('/administrador/getAdicionalesProducto/' + idProducto).then((response) => {
                $("#tallas").html(response.data.tallas)
                $("#materiales").html(response.data.materiales)
                $("#colores").html(response.data.colores)
                $("#producto").val(response.data.producto.titulo)
                $("#stock").val(response.data.producto.stock)
                $("#idProducto").val(idProducto)
            })
        }

        function storePedido() {
            axios.get('/administrador/storePedido/' + $("#idPedido").val()).then((response) => {
                if (response.data == 200) {
                    window.location.href = "/administrador/mispedidos"
                }
            })
        }

        $(document).ready(function() {
            $('#dataClient').on('click', function(e) {
                e.preventDefault();
                var dataString = $('#DataCliente').serialize();
                axios.post('/administrador/storeDataCliente', dataString).then((response) => {
                    $("#RegCliente").hide()
                    this.pedido = response.data.pedido
                    this.user = response.data.user
                    $("#idPedido").val(this.pedido.id)
                    $("#datosPedido").show()
                    $("#correoCliente").html(this.user.email)
                    $("#nombreCliente").html(this.user.name)
                    $("#idCliente").html(this.user.id)
                })
            });

            $('#storeDataProducto').on('click', function(e) {
                e.preventDefault();
                var dataString = $('#DataProducto').serialize();

                console.log(dataString)
                axios.post('/administrador/storeVariante', dataString).then((response) => {
                    $("#dataPedido").html(response.data.html)
                    $("#exampleModalCenter").modal('hide')
                    $("#cantidad").val()
                    formulario = document.getElementById('DataProducto');
                    formulario.reset();
                })
            });

            axios.get('/administrador/getVariantes/' + $("#idProducto").val()).then((response) => {
                $("#dataPedido").html(response.data.html)
                console.log("Se esta ejecutando")
            })
        });
    </script>
@endsection
