@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Crear Usuario
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

            <form action="{{ route('storeUsuario') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="">Tipo de Usuario:</label>
                    <select name="tipoUsuario" id="categoria" class="form-control">
                        <option value="1">ADMINISTRADOR</option>
                        <option value="3">DATA ENTRY</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">NOMBRE</label>
                    <input type="text" name="nombre" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">CORREO</label>
                    <input type="text" name="email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">CLAVE</label>
                    <input type="text" name="clave" id="sku" class="form-control">
                </div>


                <div class="form-group">
                    <input type="submit" class="form-control btn btn-success" value="Guardar" id="botonguardar">
                </div>
            </form>

        </div>
        <!-- /.panel-body -->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/js/jquery.multi-select.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu: "Mostrar  _MENU_  Registros",
                }
            });
        });
        $('#pre-selected-options').multiSelect();
        $('#pre-selected-options2').multiSelect();
        $('#pre-selected-options3').multiSelect();
    </script>
@endsection
