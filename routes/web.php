<?php
namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Support\Facades\Route;

Route::get('login/facebook', [Auth\LoginFacebookController::class, 'redirect']);
Route::get('login/facebook/callback', [Auth\LoginFacebookController::class, 'callback']);
Route::get('login/google', [Auth\LoginFacebookController::class, 'redirectGoogle']);
Route::get('google/callback', [Auth\LoginFacebookController::class, 'callbackGoogle']);



Route::get('/', [WelcomeController::class, 'index'])->name('welcome');
Route::get('producto/{idProducto}', [ProductoController::class, 'show'])->name('producto');
Route::get('tienda', [ProductoController::class, 'tienda'])->name('tienda');
Route::get('comprar/{idProducto}/{idVariacion}', [PedidoController::class, 'addProducto'])->name('comprar');
Route::get('eliminarProducto/{iProducto}', [RelacionPedidoProductoController::class, 'delete'])->name('eliminarProducto');
Route::get('actualizarCantidad/{idProducto}/{cantidad}', [RelacionPedidoProductoController::class, 'updateCantidad'])->name('actualizarCantidad');
Route::post('aplicarCupon', [CarritoController::class, 'aplicarCupon'])->name('aplicarCupon');
Route::get('carrito', [CarritoController::class, 'index'])->name('carrito');
Route::get('StorePedido/{totalPedido}/{idPedido}/{subTotal}', [CarritoController::class, 'updateChecout'])->name('StorePedido');
Route::get('checkout/{idPedido}', [CarritoController::class, 'checkout'])->name('checkout');
Route::post('/checkout/pago', [CarritoController::class, 'pago']);
Route::post('login_carrito', [ClienteController::class, 'login_carrito'])->name('login_carrito');
Route::get('login', [ClienteController::class, 'login'])->name('login_post');
Route::post('loginpros', [ClienteController::class, 'loginproceso'])->name('loginproceso');
Route::post('registro', [ClienteController::class, 'registro'])->name('registro');
Route::get('logout', [ClienteController::class, 'logout'])->name('logout');
Route::post('pagoYape', [CarritoController::class, 'pagoYape'])->name('pagoYape');
Route::get('provincias/{idDepartamento}', [DireccionesController::class, 'provincias']);
Route::get('distritos/{idProvincia}', [DireccionesController::class, 'distritos']);
Route::get('updateEstadoPago/{idPedido}/{estado}', [PedidoController::class, 'updateEstado'])->name('updateEstadoPago');
Route::get('filtrar', [ProductoController::class, 'filtrar'])->name('filtrar');
Route::get('nosotros', [WelcomeController::class, 'nosotros'])->name('nosotros');
Route::get('contacto', [WelcomeController::class, 'contacto'])->name('contacto');
Route::get('buscar', [ProductoController::class, 'buscador'])->name('buscar');

Route::get('video', [WelcomeController::class, 'videos'])->name('videos');

Route::get('getTallas/{idProducto}/{idColor}', [ProductoController::class, 'getTallas']);
Route::get('getMateriales/{idProducto}/{idColor}/{idTalla}', [ProductoController::class, 'getMateriales']);
Route::get('getStock/{idVariante}', [VariacionesController::class, 'stock']);



Route::group(['prefix' => 'cuenta'], function () {
    Route::get('/', [CuentaController::class, 'index'])->name('micuenta');
    Route::get('detallePedido/{idPedido}', [CuentaController::class, 'detalle_pedido'])->name('detalle_pedido');
    Route::get('cambiarclave', [CuentaController::class, 'cambiaClaveV'])->name('cambiarclave');
    Route::post('updateClave', [CuentaController::class, 'updateClave'])->name('updateClave');
});

Route::group(['prefix' => 'administrador'], function () {
    Route::get('login', [AdminController::class, 'login'])->name('loginAdmin');
    Route::post('loguearse', [AdminController::class, 'loguearse'])->name('loguearse');
    Route::get('/', [AdminController::class, 'index'])->name('indexAdmin');
    Route::get('logout', [AdminController::class, 'logout'])->name('logoutAdmin');
    Route::get('tallas', [TallaController::class, 'index'])->name('tallas');
    Route::post('storeTalla', [TallaController::class, 'store'])->name('storeTalla');
    Route::get('colores', [CloresController::class, 'index'])->name('colores');
    Route::post('storeColor', [CloresController::class, 'store'])->name('storeColor');
    Route::get('materiales', [MaterialesController::class, 'index'])->name('materiales');
    Route::post('storeMaterial', [MaterialesController::class, 'store'])->name('storeMaterial');
    Route::get('descuento', [PorcentajeDescuentoController::class, 'index'])->name('descuento');
    Route::post('storeDescuento', [PorcentajeDescuentoController::class, 'store'])->name('storeDescuento');
    Route::get('productos', [ProductoController::class, 'index'])->name('productos');
    Route::get('crearProducto', [ProductoController::class, 'create'])->name('crearProducto');
    Route::post('storeProducto', [ProductoController::class, 'store'])->name('storeProducto');
    Route::get('pedidosPA',[PedidoController::class, 'pedidosPA'])->name('pedidosPA');
    Route::get('pedidos', [PedidoController::class, 'pedidos'])->name('pedidos');
    Route::get('detallePedido/{idPedido}', [PedidoController::class, 'detallePedido'])->name('detallePedido');
    Route::post('retornarDinero', [CarritoController::class, 'retornarDinero'])->name('retornarDinero');
    Route::get('cupones', [CuponController::class, 'index'])->name('cupones');
    Route::post('storeCupon', [CuponController::class, 'store'])->name('storeCupon');
    Route::get('usuarios/{tipoUsuario}', [UserController::class, 'index'])->name('usuarios');
    Route::get('crearUsuario', [UserController::class, 'create'])->name('crearUsuario');
    Route::post('storeUsuario', [UserController::class, 'store'])->name('storeUsuario');
    Route::post('storeFactura', [CarritoController::class, 'factura'])->name('storeFactura');
    Route::get('pedidoNuevo', [PedidoController::class, 'newData'])->name('pedidoNuevo');
    Route::get('mispedidos', [PedidoController::class, 'mispedidos'])->name('mispedidos');
    Route::get('mispedidosp', [PedidoController::class, 'mispedidosp'])->name('mispedidosp');
    Route::get('mispedidospp', [PedidoController::class, 'mispedidospp'])->name('mispedidospp');
    Route::get('mispedidosdes', [PedidoController::class, 'mispedidosdes'])->name('mispedidosdes');
    Route::post('storeComprobante', [CarritoController::class, 'comprobante'])->name('storeComprobante');

    Route::get('editarstock/{idProducto}', [ProductoController::class, 'editarstock'])->name('editarstock');
    Route::post('updateStock', [ProductoController::class, 'updateStock'])->name('updateStock');
    Route::post('storeDataCliente', [UserController::class, 'storeDataCliente'])->name('storeDataCliente');
    Route::get('getAdicionalesProducto/{idProducto}', [ProductoController::class, 'getAdicionalesProducto'])->name('getAdicionalesProducto');
    Route::post('storeProductoPedido', [PedidoController::class, 'storeProductoPedido']);
    Route::get('storePedido/{idPedido}', [PedidoController::class, 'storePedido']);
    Route::get('variacionesProducto/{idProducto}', [ProductoController::class, 'variacionesProducto'])->name('variacionesProducto');
    Route::post('storeVariante', [ProductoController::class, 'storeVariante'])->name('storeVariante');
    Route::get('getVariantes/{idProducto}', [ProductoController::class, 'getVariantes'])->name('getVariantes');
    Route::post('updateProducto', [ProductoController::class, 'updateProducto'])->name('updateProducto');
});
